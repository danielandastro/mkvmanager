﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MKVManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            //MKVTNStat.Background = Brushes.LightGray;
            try { Directory.SetCurrentDirectory(loadDir());} 
            catch (Exception ex){ }
            Debug.Write(Directory.GetCurrentDirectory());
            foreach (var file in Directory.GetFiles(Directory.GetCurrentDirectory()))
            {
                Debug.Write(file);
                
                if (file.Contains("mkvtoolnix-gui.exe"))
                {
                    pathBox.Text = Directory.GetCurrentDirectory();
                    saveDir();
                }
                CheckCurrentDir();
                CheckExtract();
            }
        }

        private void pathBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void pathChangeButton_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.Filter = "mkvtoolnix-gui executable (mkvtoolnix-gui.exe)|mkvtoolnix-gui.exe";
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                string filename = dlg.FileName.Replace("mkvtoolnix-gui.exe", "");
                pathBox.Text = filename;
                Directory.SetCurrentDirectory(pathBox.Text);
                Debug.Write(Directory.GetCurrentDirectory());
                CheckCurrentDir();
                CheckExtract();
                saveDir();
            }
        }
        private void saveDir()
        {
            File.WriteAllText(System.Reflection.Assembly.GetEntryAssembly().Location+"lastdir.txt", Directory.GetCurrentDirectory());
        }
        private string loadDir()
        {
            return File.ReadAllText(System.Reflection.Assembly.GetEntryAssembly().Location +"lastdir.txt");
        }
        private void CheckExtract()
        {
            {
                bool dll = false, gui = false, json= false;
                foreach (string file in Directory.EnumerateFiles(Directory.GetCurrentDirectory()))
                {
                    Debug.Write(file);
                    if (file.Contains("gMKVExtractGUI.exe"))
                    {
                        gui = true;
                    }
                    else if (file.Contains("gMKVToolNix.dll"))
                    {
                        dll = true;
                    }
                    else if (file.Contains("Newtonsoft.Json.dll"))
                    {
                        json = true;
                    }

                    if (gui && dll && json )
                    {
                        extractStat.Foreground = Brushes.Green;
                    }
                    else
                    {
                        extractStat.Foreground = Brushes.Red;
                    }
                }
            }
        }
            private void CheckCurrentDir()
        {
            { bool extract=false, info=false, merge=false, propedit=false, gui=false;
                foreach (string file in Directory.EnumerateFiles(Directory.GetCurrentDirectory(), "*.exe"))
                {
                    Debug.Write(file);
                    if (file.Contains("mkvtoolnix-gui.exe"))
                    {
                        gui = true;
                    }
                    else if (file.Contains("mkvextract.exe"))
                    {
                        extract = true;
                    }
                    else if (file.Contains("mkvinfo.exe"))
                    {
                        info = true;
                    }
                    else if (file.Contains("mkvmerge.exe"))
                    {
                        merge = true;
                    }
                    else if (file.Contains("mkvpropedit.exe"))
                    {
                        propedit = true;
                    }

                    if (gui && propedit && merge && info && extract)
                    {
                        MKVTNStat.Foreground = Brushes.Green;
                    }
                    else if (gui || propedit || merge || info || extract)
                    {
                        MKVTNStat.Foreground = Brushes.Orange;
                    }
                    else
                    {
                        MKVTNStat.Foreground = Brushes.Red;
                    }
                }
            }
        }
    }
}